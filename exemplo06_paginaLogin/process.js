const {createApp} = Vue;

createApp({
    data(){
        return{
            username:"",
            password:"",
            error: null,
            sucesso: null,
            userAdmin: false,

            //Declaração dos arrays para armazenamento de usuários e senhas
            usuarios: ["ali", "henri", "pessoa1"],
            senhas:["1234", "1234", "1234"],
        }
    },//Fechamento data

    methods:{
        login(){
            setTimeout(() => {
                // alert("Dentro do setTimeout!");
                if((this.username === "henri" && this.password === "1234") ||
                (this.username === "pessoa1" && this.password === "1234") ||
                (this.username === "ali" && this.password === "1234")){
                    this.sucesso = "Login efetuado com sucesso!";
                    this.error = null;
                    if(this.username === "Admin"){
                        this.userAdmin = true;
                    }
                }
                else{
                    // alert("Login não efetuado!");
                    this.error = "Nome ou senha incorretos!";
                    this.sucesso = null;
                }
            }, 1000);
            // alert("Saiu do setTimeout!");
        },//Fechamento Login

        login2(){
            const index = this.usuarios.indexOf(this.username);
            if(index !== -1 && this.senhas[index] === this.password){
                this.error = null;
                this.sucesso = "Login efetuado com sucesso!";
                if(this.username === "ali" && index === 0){
                    this.userAdmin = true;
                }
            }//Fechamento if

            else{
                this.error = "Nome ou senha incorretos!"
            }//Fechamento else

        },//Fechamento login2

        paginaCadastro(){
            if(this.userAdmin == true){
                this.sucesso= "Login ADM identificado!";

                localStorage.setItem("userAdmin", this.userAdmin);

                setTimeout(() => {
                    window.location.href = "paginaCadastro.html";
                },1000);//Fechamento setTimeout

            }
            else{
               this.error = "Faça login com usuário administrativo!";
            }
        },//Fechamento paginaCadastro

        adicionarUsuario(){
            this.userAdmin = localStorage.getItem("userAdmin");

           if(this.userAdmin === true){
                this.sucesso = "Logado como ADM!!!";
                this.error = null;
            }//Fechamento if
            else{
                this.error = "Usuário NÃO ADM!!!";
                this.sucesso = null;
            }

        },//Fechamento adicionarUsuario
    },//Fechamento methods

}).mount("#app");