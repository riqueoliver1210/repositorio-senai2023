const {createApp} = Vue;

createApp({
    data(){
        return{
            display:"0",
            operador: null,
            numeroAtual: null,
            numeroAnterior: null,
            tamanhoLetra: 50 + "px",
            tamanhoDisplay: 10,
        };
    },//Fechamento data

    methods:{
        getnNumero(valor){
            this.ajusteTamahoDisplay();
            if (this.display == "0"){
                this.display = valor.toString();
            }

            else if(this.display.length >= this.tamanhoDisplay){
                alert("Ta em narnia?");
            }

            else{

                if(this.operador == "=" || this.valorDisplay.includes("op")){
                    this.display = '';
                    this.operador = null;
                }

                //this.display = this.display + valor.toString();

                //Fórmula resumida:
                this.display += valor.toString();
            }
        },//Fechamento numero  
        
        decimal(){
            if(!this.display.includes(".")){
           // this.display = this.display + ".";

            //Fórmula resumida:
            this.display += ".";
            }
        },//Fechamento decimal

        clear(){
            this.display = "0";
            this.numeroAtual = null;
            this.numeroAnterior = null;
            this.operador = null;
        },//Fechamento clear

        operacoes(operacao){
            if(this.operador != null){
                const displayAtual = parseFloat(this.display);
                switch(this.operador){
                    case "+":
                        this.display = (this.numeroAtual + displayAtual).toString();
                        break;
                    case "-":
                        this.display = (this.numeroAtual - displayAtual).toString();
                        break;
                    case "*":
                        this.display = (this.numeroAtual * displayAtual).toString();
                        break;
                    case "/":
                        this.display = (this.numeroAtual / displayAtual).toString();
                        break;  
                }//Fim do switch
                
                this.numeroAnterior = this.numeroAtual;
                this.numeroAtual = null;
                

                if(this.operador != "="){
                    this.operador = null;
                }

                if(this.valorDisplay.includes(".")){
                    const numDecimal = parseFloat(this.valorDisplay);
                    this.valorDisplay = (numDecimal . toFixed(2)).toString();

                }
            }//Fim do if

            if(operacao != "="){
                this.operador = operacao;
                this.numeroAtual = parseFloat(this.display);
                this.display = "0";
            }
            else{
                this.operador = operacao;

                if(this.display.length >= this.tamanhoDisplay){
                    alert("ta em narnia?");
                }

                if(this.display.includes(".")){
                    const displayAtual = parseFloat (this.display);
                    (this.display = displayAtual.toFixed(2)).toString();
                }
                 // alert("O número atual é: " + this.numeroAtual);
                        
            }
            this.ajusteTamahoDisplay();
            // alert("O operador é: " + this.operador);
        },
        ajusteTamahoDisplay(){
            if(this.valorDisplay.length >= 31){
                this.tamanhoLetra = 14 + "px";
            }
            else if(this.valorDisplay.length >= 21){
                this.tamanhoLetra = 20 + "px";
            }
            else if(this.valorDisplay.length >= 12){
                this.tamanhoLetra = 30 + "px";
            }
            else{
                this.tamanhoLetra = 50 + "px";
            }            
        }
    },//Fechamento methods

}).mount("#app");//Fechamento createApp